<?php
if (!defined('ABSPATH')) die();
require_once(ABSPATH . 'wp-admin/includes/taxonomy.php');


function usp_add_options_page() {
	add_options_page(USP_PLUGIN, USP_PLUGIN, 'manage_options', USP_PATH, 'usp_render_form');
}
add_action('admin_menu', 'usp_add_options_page');



function usp_init() {
	register_setting('usp_plugin_options', 'usp_options', 'usp_validate_options');
}
add_action('admin_init', 'usp_init');

function usp_post_type() {
	
	$post_type = array(
		
		'post' => array(
			'value' => 'post',
			'label' => esc_html__('WP Post (recommended)', 'usp'),
		),
		'page' => array(
			'value' => 'page',
			'label' => esc_html__('WP Page', 'usp'),
		),
	);
	
	return $post_type;
	
}




function usp_image_display() {
	
	$image_display = array(
		
		'before' => array(
			'value' => 'before',
			'label' => esc_html__('Auto-display before post content', 'usp')
		),
		'after' => array(
			'value' => 'after',
			'label' => esc_html__('Auto-display after post content', 'usp')
		),
		'disable' => array(
			'value' => 'disable',
			'label' => esc_html__('Do not auto-display submitted images', 'usp')
		),
	);
	
	return $image_display;
	
}



function usp_email_display() {
	
	$email_display = array(
		
		'before' => array(
			'value' => 'before',
			'label' => esc_html__('Auto-display before post content', 'usp')
		),
		'after' => array(
			'value' => 'after',
			'label' => esc_html__('Auto-display after post content', 'usp')
		),
		'disable' => array(
			'value' => 'disable',
			'label' => esc_html__('Do not auto-display submitted email', 'usp')
		),
	);
	
	return $email_display;
	
}

function usp_form_field_options($args) {
	
	global $usp_options;
	
	$name  = isset($args[0]) ? $args[0] : '';
	$label = isset($args[1]) ? $args[1] : '';
	
	$option = isset($usp_options[$name]) ? $usp_options[$name] : '';
	
	$selected_show = ($option === 'show') ? 'selected="selected"' : '';
	$selected_optn = ($option === 'optn') ? 'selected="selected"' : '';
	$selected_hide = ($option === 'hide') ? 'selected="selected"' : '';
	
	$output  = '<tr>';
	$output .= '<th scope="row"><label class="description" for="usp_options['. esc_attr($name) .']">'. esc_html($label) .'</label></th>';
	$output .= '<td>';
	$output .= '<select name="usp_options['. esc_attr($name) .']" id="usp_options['. esc_attr($name) .']">';
	
	$output .= '<option '. $selected_show .' value="show">'. esc_html__('Display and require', 'usp')        .'</option>';
	$output .= '<option '. $selected_optn .' value="optn">'. esc_html__('Display but do not require', 'usp') .'</option>';
	$output .= '<option '. $selected_hide .' value="hide">'. esc_html__('Disable this field', 'usp')         .'</option>';
	
	$output .= '</select>';
	$output .= '</td>';
	$output .= '</tr>';
	
	return $output;
	
}



function usp_form_field_options_custom() {
	
	global $usp_options;
	
	$name  = 'custom_field';
	$label = esc_html__('Custom Field', 'usp');
	
	$option = isset($usp_options[$name]) ? $usp_options[$name] : '';
	
	$selected_show = ($option === 'show') ? 'selected="selected"' : '';
	$selected_optn = ($option === 'optn') ? 'selected="selected"' : '';
	$selected_hide = ($option === 'hide') ? 'selected="selected"' : '';
	
	$output  = '<tr>';
	$output .= '<th scope="row"><label class="description" for="usp_options['. esc_attr($name) .']">'. esc_html($label) .'</label></th>';
	$output .= '<td>';
	$output .= '<select name="usp_options['. esc_attr($name) .']" id="usp_options['. esc_attr($name) .']">';
	
	$output .= '<option '. $selected_show .' value="show">'. esc_html__('Display and require', 'usp')        .'</option>';
	$output .= '<option '. $selected_optn .' value="optn">'. esc_html__('Display but do not require', 'usp') .'</option>';
	$output .= '<option '. $selected_hide .' value="hide">'. esc_html__('Disable this field', 'usp')         .'</option>';
	
	$output .= '</select> ';
	$output .= '<span class="mm-item-caption">'. esc_html__('(Visit', 'usp') .' <a href="#usp-custom-field">';
	$output .= esc_html__('Custom Field', 'usp') .'</a> '. esc_html__('to configure options)', 'usp') .'</span>';
	$output .= '</td>';
	$output .= '</tr>';
	
	return $output;
	
}


function usp_form_field_options_recaptcha() {
	
	global $usp_options;
	
	$name  = 'usp_recaptcha';
	$label = esc_html__('Google reCAPTCHA', 'usp');
	
	$option = isset($usp_options[$name]) ? $usp_options[$name] : '';
	
	$selected_show = ($option === 'show') ? 'selected="selected"' : '';
	$selected_hide = ($option === 'hide') ? 'selected="selected"' : '';
	
	$output  = '<tr>';
	$output .= '<th scope="row"><label class="description" for="usp_options['. esc_attr($name) .']">'. esc_html($label) .'</label></th>';
	$output .= '<td>';
	$output .= '<select name="usp_options['. esc_attr($name) .']" id="usp_options['. esc_attr($name) .']">';
	
	$output .= '<option '. $selected_show .' value="show">'. esc_html__('Display and require', 'usp') .'</option>';
	$output .= '<option '. $selected_hide .' value="hide">'. esc_html__('Disable this field', 'usp')  .'</option>';
	
	$output .= '</select> ';
	$output .= '<span class="mm-item-caption">'. esc_html__('(Visit', 'usp') .' <a href="#usp-recaptcha">';
	$output .= esc_html__('Google reCAPTCHA', 'usp') .'</a> '. esc_html__('to configure options)', 'usp') .'</span>';
	$output .= '</td>';
	$output .= '</tr>';
	
	return $output;
	
}



function usp_form_field_options_images() {
	
	global $usp_options;
	
	$name  = 'usp_images';
	$label = esc_html__('Post Images', 'usp');
	
	$option = isset($usp_options[$name]) ? $usp_options[$name] : '';
	
	$selected_show = ($option === 'show') ? 'selected="selected"' : '';
	$selected_hide = ($option === 'hide') ? 'selected="selected"' : '';
	
	$output  = '<tr>';
	$output .= '<th scope="row"><label class="description" for="usp_options['. esc_attr($name) .']">'. esc_html($label) .'</label></th>';
	$output .= '<td>';
	$output .= '<select name="usp_options['. esc_attr($name) .']" id="usp_options['. esc_attr($name) .']">';
	
	$output .= '<option '. $selected_show .' value="show">'. esc_html__('Display', 'usp') .'</option>';
	$output .= '<option '. $selected_hide .' value="hide">'. esc_html__('Disable', 'usp') .'</option>';
	
	$output .= '</select> ';
	$output .= '<span class="mm-item-caption">'. esc_html__('(Visit', 'usp') .' <a href="#usp-image-uploads">';
	$output .= esc_html__('Image Uploads', 'usp') .'</a> '. esc_html__('to configure options)', 'usp') .'</span>';
	$output .= '</td>';
	$output .= '</tr>';
	
	return $output;
	
}




function usp_post_type_options() {
	
	global $usp_options;
	
	$usp_post_type = usp_post_type();
	
	$selected_option = isset($usp_options['usp_post_type']) ? $usp_options['usp_post_type'] : 'post';
	
	$select_options = '<select name="usp_options[usp_post_type]">';
	
	foreach($usp_post_type as $k => $v) {
		
		$selected = selected($selected_option === $k, true, false);
		
		$value = isset($v['value']) ? $v['value'] : null;
		$label = isset($v['label']) ? $v['label'] : null;
		
		$select_options .= '<option value="'. $value .'"'. $selected .'>'. $label .'</option>';
		
	}
	
	$select_options .= '</select>';		
	
	return $select_options;
	
}



function usp_post_status_options() {
	
	global $usp_options;
	
	$approved = isset($usp_options['number-approved']) ? intval($usp_options['number-approved']) : -1;
	
	$output = '<select id="usp_options[number-approved]" name="usp_options[number-approved]">';
	
	foreach (range(-2, 20) as $v) {
		
		if     ($v === -2) $k = esc_html__('Draft', 'usp');	
		elseif ($v === -1) $k = esc_html__('Pending (default)', 'usp');
		elseif ($v === 0)  $k = esc_html__('Publish immediately', 'usp');
		elseif ($v === 1)  $k = esc_html__('Publish after 1 approved post', 'usp');
		else               $k = esc_html__('Publish after ', 'usp') . $v . esc_html__(' approved posts', 'usp');
		
		$selected = selected($v, $approved, false);
		
		$output .= '<option '. $selected .' value="'. $v .'">'. $k .'</option>';
		
	}
	
	$output .= '</select>';
	
	return $output;
	
}



function usp_post_author_options() {
	
	global $usp_options, $wpdb;
	
	$user_count = count_users();
	
	$user_total = isset($user_count['total_users']) ? intval($user_count['total_users']) : 1;
	
	$user_max = apply_filters('usp_max_users', 200);
	
	$limit = ($user_total > $user_max) ? $user_max : $user_total;
	
	$query = "SELECT ID, display_name FROM {$wpdb->users} LIMIT %d";
	
	$users = $wpdb->get_results($wpdb->prepare($query, $limit));
	
	$output = '<select id="usp_options[author]" name="usp_options[author]">';
	
	foreach($users as $user) {
		
		$selected = isset($usp_options['author']) ? selected($usp_options['author'], $user->ID, false) : '';
		
		$output .= '<option '. $selected .'value="'. esc_attr($user->ID) .'">'. esc_html($user->display_name) .'</option>';
		
	}
	
	$output .= '</select>';
	
	return $output;
	
}



function usp_auto_display_options($item) {
	
	global $usp_options;
	
	$usp_image_display  = usp_image_display();
	$usp_email_display  = usp_email_display();
	$usp_url_display    = usp_url_display();
	$usp_custom_display = usp_custom_display();
	
	if ($item === 'images') {
		
		$array = $usp_image_display;
		$key = 'auto_display_images';
		
	} elseif ($item === 'email') {
		
		$array = $usp_email_display;
		$key = 'auto_display_email';
		
	} elseif ($item === 'url') {
		
		$array = $usp_url_display;
		$key = 'auto_display_url';
		
	} elseif ($item === 'custom') {
		
		$array = $usp_custom_display;
		$key = 'auto_display_custom';
	}
	
	$radio_setting = isset($usp_options[$key]) ? $usp_options[$key] : '';
	
	$output = '';
	
	foreach ($array as $arr) {
		
		$label = isset($arr['label']) ? $arr['label'] : '';
		$value = isset($arr['value']) ? $arr['value'] : '';
		
		$checked = (!empty($radio_setting) && $radio_setting === $value) ? 'checked="checked"' : '';
		
		$output .= '<div class="mm-radio-inputs">';
		$output .= '<input type="radio" name="usp_options['. esc_attr($key) .']" value="'. esc_attr($value) .'" '. $checked .' /> '. esc_html($label);
		$output .= '</div>';
		
	}
	
	return $output;
	
}



function usp_add_defaults() {
	
	$currentUser = wp_get_current_user();
	
	$admin_mail = get_bloginfo('admin_email');
	
	$tmp = get_option('usp_options');
	
	if (($tmp['default_options'] == '1') || (!is_array($tmp))) {
		
		$arr = array(
			'author'              => $currentUser->ID,
			'error-message'       => esc_html__('Ошибка. Пожалуйста, проверьте, все ли поля заполнены правильно?', 'usp'),
			'usp_name'            => 'show',
			'usp_url'             => 'show',
			'usp_email'           => 'hide',
			'usp_title'           => 'show',
			'upload-message'      => esc_html__('Пожалуйста, выберите изображение', 'usp'),
			'usp_content'         => 'show',
			'success-message'     => esc_html__('Спасибо! Ваш вопрос отправлен.', 'usp'),
			'text_page'     	  => esc_html__('Текст на странице Вопрос-Ответ', 'usp'),
			'usp_email_alerts'    => 1,
			'usp_email_html'      => 0,
			'usp_email_address'   => $admin_mail,
			'usp_email_from'      => $admin_mail,
			'usp_use_author'      => 0,
			'usp_use_url'         => 0,
			'usp_use_cat'         => 0,
			'usp_use_cat_id'      => '',
			'usp_include_js'      => 1,
			'usp_display_url'     => '',
			'usp_form_content'    => '',
			'usp_richtext_editor' => 0,
			'usp_featured_images' => 0,
			'usp_add_another'     => '',
			'disable_required'    => 0,
			'titles_unique'       => 0,
			'enable_shortcodes'   => 0,
			'disable_ip_tracking' => 0,
			'email_alert_subject' => '',
			'email_alert_message' => '',
			'auto_display_images' => 'disable',
			'auto_display_email'  => 'disable', 
			'auto_display_url'    => 'disable', 
			'auto_image_markup'   => '<a href="%%full%%"><img src="%%thumb%%" width="%%width%%" height="%%height%%" alt="%%title%%" style="display:inline-block;" /></a> ',
			'auto_email_markup'   => '<p><a href="mailto:%%email%%">'. esc_html__('Email', 'usp') .'</a></p>',
			'auto_url_markup'     => '<p><a href="%%url%%">'. esc_html__('URL', 'usp') .'</a></p>',
			'logged_in_users'     => 0,
			'recaptcha_public'    => '',
			'recaptcha_private'   => '',
			'usp_recaptcha'       => 'hide',
			'usp_post_type'       => 'post',
			'custom_field'        => 'hide',
			'custom_name'         => 'usp_custom_field',
			'custom_label'        => esc_html__('Custom Field', 'usp'),
			'auto_display_custom' => 'disable',
			'auto_custom_markup'  => '<p>%%custom_label%% : %%custom_name%% : %%custom_value%%</p>',
		);
		
		update_option('usp_options', $arr);
		
	}
	
}
register_activation_hook(dirname(dirname(__FILE__)).'/user-submitted-posts.php', 'usp_add_defaults');



function usp_delete_plugin_options() {
	
	delete_option('usp_options');
	
}
if ($usp_options['default_options'] == 1) {
	
	register_deactivation_hook(dirname(dirname(__FILE__)).'/user-submitted-posts.php', 'usp_delete_plugin_options');
	
}



function usp_validate_options($input) {
	
	global $usp_options;
	
	if (!isset($input['version_alert'])) $input['version_alert'] = null;
	$input['version_alert'] = ($input['version_alert'] == 1 ? 1 : 0);
	
	if (!isset($input['default_options'])) $input['default_options'] = null;
	$input['default_options'] = ($input['default_options'] == 1 ? 1 : 0);
	
	$input['number-approved']  = is_numeric($input['number-approved']) ? intval($input['number-approved']) : -1;
	
	$input['min-images']       = is_numeric($input['min-images']) ? intval($input['min-images']) : $input['max-images'];
	$input['max-images']       = (is_numeric($input['max-images']) && ($usp_options['min-images'] <= abs($input['max-images']))) ? intval($input['max-images']) : $usp_options['max-images'];
	
	$input['min-image-height'] = is_numeric($input['min-image-height']) ? intval($input['min-image-height']) : $usp_options['min-image-height'];
	$input['min-image-width']  = is_numeric($input['min-image-width'])  ? intval($input['min-image-width'])  : $usp_options['min-image-width'];
	
	$input['max-image-height'] = (is_numeric($input['max-image-height']) && ($usp_options['min-image-height'] <= $input['max-image-height'])) ? intval($input['max-image-height']) : $usp_options['max-image-height'];
	$input['max-image-width']  = (is_numeric($input['max-image-width'])  && ($usp_options['min-image-width']  <= $input['max-image-width']))  ? intval($input['max-image-width'])  : $usp_options['max-image-width'];
	

	$usp_image_display = usp_image_display();
	if (!isset($input['auto_display_images'])) $input['auto_display_images'] = null;
	if (!array_key_exists($input['auto_display_images'], $usp_image_display)) $input['auto_display_images'] = null;
	
	$usp_email_display = usp_email_display();
	if (!isset($input['auto_display_email'])) $input['auto_display_email'] = null;
	if (!array_key_exists($input['auto_display_email'], $usp_email_display)) $input['auto_display_email'] = null;
	
	$usp_post_type = usp_post_type();
	if (!isset($input['usp_post_type'])) $input['usp_post_type'] = null;
	if (!array_key_exists($input['usp_post_type'], $usp_post_type)) $input['usp_post_type'] = null;
	
	if (isset($input['author']))              $input['author']              = wp_filter_nohtml_kses($input['author']);              else $input['author']              = null;
	if (isset($input['usp_name']))            $input['usp_name']            = wp_filter_nohtml_kses($input['usp_name']);            else $input['usp_name']            = null;
	if (isset($input['usp_url']))             $input['usp_url']             = wp_filter_nohtml_kses($input['usp_url']);             else $input['usp_url']             = null; 
	if (isset($input['usp_email']))           $input['usp_email']           = wp_filter_nohtml_kses($input['usp_email']);           else $input['usp_email']           = null;
	if (isset($input['usp_title']))           $input['usp_title']           = wp_filter_nohtml_kses($input['usp_title']);           else $input['usp_title']           = null;
	if (isset($input['usp_tags']))            $input['usp_tags']            = wp_filter_nohtml_kses($input['usp_tags']);            else $input['usp_tags']            = null;
	if (isset($input['usp_images']))          $input['usp_images']          = wp_filter_nohtml_kses($input['usp_images']);          else $input['usp_images']          = null;
	if (isset($input['usp_question']))        $input['usp_question']        = wp_filter_nohtml_kses($input['usp_question']);        else $input['usp_question']        = null;
	if (isset($input['usp_captcha']))         $input['usp_captcha']         = wp_filter_nohtml_kses($input['usp_captcha']);         else $input['usp_captcha']         = null;
	if (isset($input['usp_content']))         $input['usp_content']         = wp_filter_nohtml_kses($input['usp_content']);         else $input['usp_content']         = null;
	if (isset($input['usp_email_address']))   $input['usp_email_address']   = wp_filter_nohtml_kses($input['usp_email_address']);   else $input['usp_email_address']   = null;
	if (isset($input['usp_email_from']))      $input['usp_email_from']      = wp_filter_nohtml_kses($input['usp_email_from']);      else $input['usp_email_from']      = null;
	if (isset($input['usp_use_cat_id']))      $input['usp_use_cat_id']      = wp_filter_nohtml_kses($input['usp_use_cat_id']);      else $input['usp_use_cat_id']      = null;
	if (isset($input['usp_display_url']))     $input['usp_display_url']     = wp_filter_nohtml_kses($input['usp_display_url']);     else $input['usp_display_url']     = null;
	if (isset($input['redirect-url']))        $input['redirect-url']        = wp_filter_nohtml_kses($input['redirect-url']);        else $input['redirect-url']        = null;
	if (isset($input['email_alert_subject'])) $input['email_alert_subject'] = wp_filter_nohtml_kses($input['email_alert_subject']); else $input['email_alert_subject'] = null;
	if (isset($input['recaptcha_public']))    $input['recaptcha_public']    = wp_filter_nohtml_kses($input['recaptcha_public']);    else $input['recaptcha_public']    = null;
	if (isset($input['recaptcha_private']))   $input['recaptcha_private']   = wp_filter_nohtml_kses($input['recaptcha_private']);   else $input['recaptcha_private']   = null;
	if (isset($input['usp_recaptcha']))       $input['usp_recaptcha']       = wp_filter_nohtml_kses($input['usp_recaptcha']);       else $input['usp_recaptcha']       = null;
	if (isset($input['custom_field']))        $input['custom_field']        = wp_filter_nohtml_kses($input['custom_field']);        else $input['custom_field']        = null;
	if (isset($input['custom_name']))         $input['custom_name']         = wp_filter_nohtml_kses($input['custom_name']);         else $input['custom_name']         = null;
	if (isset($input['custom_label']))        $input['custom_label']        = wp_filter_nohtml_kses($input['custom_label']);        else $input['custom_label']        = null;
	
	// dealing with kses
	global $allowedposttags;
	$allowed_atts = array(
		'align'      => array(),
		'class'      => array(),
		'type'       => array(),
		'id'         => array(),
		'dir'        => array(),
		'lang'       => array(),
		'style'      => array(),
		'xml:lang'   => array(),
		'src'        => array(),
		'alt'        => array(),
		'href'       => array(),
		'rel'        => array(),
		'rev'        => array(),
		'target'     => array(),
		'novalidate' => array(),
		'type'       => array(),
		'value'      => array(),
		'name'       => array(),
		'tabindex'   => array(),
		'action'     => array(),
		'method'     => array(),
		'for'        => array(),
		'width'      => array(),
		'height'     => array(),
		'data'       => array(),
		'title'      => array(),
	);
	$allowedposttags['form']     = $allowed_atts;
	$allowedposttags['label']    = $allowed_atts;
	$allowedposttags['input']    = $allowed_atts;
	$allowedposttags['textarea'] = $allowed_atts;
	$allowedposttags['iframe']   = $allowed_atts;
	$allowedposttags['script']   = $allowed_atts;
	$allowedposttags['style']    = $allowed_atts;
	$allowedposttags['strong']   = $allowed_atts;
	$allowedposttags['small']    = $allowed_atts;
	$allowedposttags['table']    = $allowed_atts;
	$allowedposttags['span']     = $allowed_atts;
	$allowedposttags['abbr']     = $allowed_atts;
	$allowedposttags['code']     = $allowed_atts;
	$allowedposttags['pre']      = $allowed_atts;
	$allowedposttags['div']      = $allowed_atts;
	$allowedposttags['img']      = $allowed_atts;
	$allowedposttags['h1']       = $allowed_atts;
	$allowedposttags['h2']       = $allowed_atts;
	$allowedposttags['h3']       = $allowed_atts;
	$allowedposttags['h4']       = $allowed_atts;
	$allowedposttags['h5']       = $allowed_atts;
	$allowedposttags['h6']       = $allowed_atts;
	$allowedposttags['ol']       = $allowed_atts;
	$allowedposttags['ul']       = $allowed_atts;
	$allowedposttags['li']       = $allowed_atts;
	$allowedposttags['em']       = $allowed_atts;
	$allowedposttags['hr']       = $allowed_atts;
	$allowedposttags['br']       = $allowed_atts;
	$allowedposttags['tr']       = $allowed_atts;
	$allowedposttags['td']       = $allowed_atts;
	$allowedposttags['p']        = $allowed_atts;
	$allowedposttags['a']        = $allowed_atts;
	$allowedposttags['b']        = $allowed_atts;
	$allowedposttags['i']        = $allowed_atts;
	
	if (isset($input['usp_form_content']))    $input['usp_form_content']    = wp_kses_post($input['usp_form_content'],    $allowedposttags); else $input['usp_form_content']    = null;
	if (isset($input['error-message']))       $input['error-message']       = wp_kses_post($input['error-message'],       $allowedposttags); else $input['error-message']       = null;
	if (isset($input['upload-message']))      $input['upload-message']      = wp_kses_post($input['upload-message'],      $allowedposttags); else $input['upload-message']      = null;
	if (isset($input['success-message']))     $input['success-message']     = wp_kses_post($input['success-message'],     $allowedposttags); else $input['success-message']     = null;
	if (isset($input['usp_add_another']))     $input['usp_add_another']     = wp_kses_post($input['usp_add_another'],     $allowedposttags); else $input['usp_add_another']     = null;
	if (isset($input['email_alert_message'])) $input['email_alert_message'] = wp_kses_post($input['email_alert_message'], $allowedposttags); else $input['email_alert_message'] = null;
	if (isset($input['auto_image_markup']))   $input['auto_image_markup']   = wp_kses_post($input['auto_image_markup'],   $allowedposttags); else $input['auto_image_markup']   = null;
	if (isset($input['auto_email_markup']))   $input['auto_email_markup']   = wp_kses_post($input['auto_email_markup'],   $allowedposttags); else $input['auto_email_markup']   = null;
	if (isset($input['auto_url_markup']))     $input['auto_url_markup']     = wp_kses_post($input['auto_url_markup'],     $allowedposttags); else $input['auto_url_markup']     = null;
	if (isset($input['auto_custom_markup']))  $input['auto_custom_markup']  = wp_kses_post($input['auto_custom_markup'],  $allowedposttags); else $input['auto_custom_markup']  = null;
	
	if (!isset($input['usp_casing'])) $input['usp_casing'] = null;
	$input['usp_casing'] = ($input['usp_casing'] == 1 ? 1 : 0);
	
	if (!isset($input['usp_email_alerts'])) $input['usp_email_alerts'] = null;
	$input['usp_email_alerts'] = ($input['usp_email_alerts'] == 1 ? 1 : 0);
	
	if (!isset($input['usp_email_html'])) $input['usp_email_html'] = null;
	$input['usp_email_html'] = ($input['usp_email_html'] == 1 ? 1 : 0);
	
	if (!isset($input['usp_use_author'])) $input['usp_use_author'] = null;
	$input['usp_use_author'] = ($input['usp_use_author'] == 1 ? 1 : 0);
	
	if (!isset($input['usp_use_url'])) $input['usp_use_url'] = null;
	$input['usp_use_url'] = ($input['usp_use_url'] == 1 ? 1 : 0);
	
	if (!isset($input['usp_use_cat'])) $input['usp_use_cat'] = null;
	$input['usp_use_cat'] = ($input['usp_use_cat'] == 1 ? 1 : 0);
	
	if (!isset($input['usp_include_js'])) $input['usp_include_js'] = null;
	$input['usp_include_js'] = ($input['usp_include_js'] == 1 ? 1 : 0);
	
	if (!isset($input['usp_richtext_editor'])) $input['usp_richtext_editor'] = null;
	$input['usp_richtext_editor'] = ($input['usp_richtext_editor'] == 1 ? 1 : 0);
	
	if (!isset($input['usp_featured_images'])) $input['usp_featured_images'] = null;
	$input['usp_featured_images'] = ($input['usp_featured_images'] == 1 ? 1 : 0);
	
	if (!isset($input['disable_required'])) $input['disable_required'] = null;
	$input['disable_required'] = ($input['disable_required'] == 1 ? 1 : 0);
	
	if (!isset($input['titles_unique'])) $input['titles_unique'] = null;
	$input['titles_unique'] = ($input['titles_unique'] == 1 ? 1 : 0);
	
	if (!isset($input['enable_shortcodes'])) $input['enable_shortcodes'] = null;
	$input['enable_shortcodes'] = ($input['enable_shortcodes'] == 1 ? 1 : 0);
	
	if (!isset($input['disable_ip_tracking'])) $input['disable_ip_tracking'] = null;
	$input['disable_ip_tracking'] = ($input['disable_ip_tracking'] == 1 ? 1 : 0);
	
	if (!isset($input['logged_in_users'])) $input['logged_in_users'] = null;
	$input['logged_in_users'] = ($input['logged_in_users'] == 1 ? 1 : 0);
	
	return apply_filters('usp_input_validate', $input);
	
}



function usp_render_form() {
	
	global $usp_options, $wpdb; 
	
	
	$display_alert = (isset($usp_options['version_alert']) && $usp_options['version_alert']) ? ' style="display:none;"' : ' style="display:block;"'; 
	

	?>
	
	<style type="text/css">#mm-plugin-options .usp-custom-form-info { <?php echo $custom_styles; ?> }</style>
	
	<div id="mm-plugin-options" class="wrap">
		
		<h1><?php echo USP_PLUGIN; ?></h1>
		
		<form method="post" action="options.php">
			<?php settings_fields('usp_plugin_options'); ?>
			
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div id="mm-panel-primary" class="postbox" style="padding: 20px">
						<h2>Настройки плагина</h2>
						<div>							
							<h3>Поля формы</h3>
							
							<div class="mm-table-wrap mm-table-less-padding">
								<table class="widefat mm-table">
									<?php 
										
										echo usp_form_field_options(array('usp_name',     esc_html__('User Name',     'usp')));
										echo usp_form_field_options(array('usp_email',    esc_html__('User Email',    'usp')));
										echo usp_form_field_options(array('usp_title',    esc_html__('Post Title',    'usp')));
										echo usp_form_field_options(array('usp_content',  esc_html__('Post Content',  'usp')));
										
										echo usp_form_field_options_recaptcha();
										echo usp_form_field_options_images();
										
									?>
								</table>
							</div>
							
							<h3>Общие настройки</h3>
							<div class="mm-table-wrap">
								<table class="widefat mm-table">
									<tr>
										<th scope="row"><label class="description" for="usp_options[number-approved]"><?php esc_html_e('Post Status', 'usp'); ?></label></th>
										<td>
											<?php echo usp_post_status_options(); ?>
											<div class="mm-item-caption"><?php esc_html_e('Post Status for submitted posts', 'usp'); ?></div>
										</td>
									</tr>
									<tr>
										<th scope="row"><label class="description" for="usp_options[success-message]"><?php esc_html_e('Success Message', 'usp'); ?></label></th>
										<td><textarea class="textarea" rows="3" cols="50" name="usp_options[success-message]"><?php if (isset($usp_options['success-message'])) echo esc_textarea($usp_options['success-message']); ?></textarea>
										<div class="mm-item-caption">Например: Спасибо! Ваш вопрос отправлен.</div>
										</td>

									</tr>
									<tr>
										<th scope="row"><label class="description" for="usp_options[error-message]"><?php esc_html_e('Error Message', 'usp'); ?></label></th>
										<td><textarea class="textarea" rows="3" cols="50" name="usp_options[error-message]"><?php if (isset($usp_options['error-message'])) echo esc_textarea($usp_options['error-message']); ?></textarea> 
										<div class="mm-item-caption">Например: Ошибка. Пожалуйста, проверьте, все ли поля заполнены правильно?</div></td>
									</tr>
									<tr>
										<th scope="row"><label class="description" for="usp_options[text_page]">Текст на странице "Вопрос-Ответ"</label></th>
										<td><textarea class="textarea" rows="3" cols="50" name="usp_options[text_page]"><?php if (isset($usp_options['text_page'])) echo esc_textarea($usp_options['text_page']); ?></textarea>
											<div class="mm-item-caption">Например: Задайте вопрос эксперту, используя форму ниже. Постарайтесь изложить проблему максимально подробно.</div></td>
									</tr>
								</table>
							</div>
							<h3>Автор</h3>
							<div class="mm-table-wrap">
								<table class="widefat mm-table">
									<tr>
										<th scope="row"><label class="description" for="usp_options[author]"><?php esc_html_e('Assigned Author', 'usp'); ?></label></th>
										<td>
											<?php echo usp_post_author_options(); ?>
											<span class="mm-item-caption"><?php esc_html_e('Specify the user that should be assigned as author for submitted posts.', 'usp'); ?></span>
										</td>
									</tr>
								</table>
							</div>
								
							
							<h3 id="usp-recaptcha">Google reCAPTCHA</h3>
							<div class="mm-table-wrap">
								<table class="widefat mm-table">
									<tr>
										<th scope="row"><label class="description" for="usp_options[recaptcha_public]"><?php esc_html_e('Public Key', 'usp'); ?></label></th>
										<td><input type="text" size="45" name="usp_options[recaptcha_public]" value="<?php if (isset($usp_options['recaptcha_public'])) echo esc_attr($usp_options['recaptcha_public']); ?>" />
										<div class="mm-item-caption"><?php esc_html_e('Enter your Public Key', 'usp'); ?></div></td>
									</tr>
									<tr>
										<th scope="row"><label class="description" for="usp_options[recaptcha_private]"><?php esc_html_e('Private Key', 'usp'); ?></label></th>
										<td><input type="text" size="45" name="usp_options[recaptcha_private]" value="<?php if (isset($usp_options['recaptcha_private'])) echo esc_attr($usp_options['recaptcha_private']); ?>" />
										<div class="mm-item-caption"><?php esc_html_e('Enter your Private Key', 'usp'); ?></div></td>
									</tr>
								</table>
							</div>

							<h3 id="usp-image-uploads">Загрузка изображений</h3>
							<div class="mm-table-wrap">
								<table class="widefat mm-table">
									<tr>
										<th scope="row"><label class="description" for="usp_options[upload-message]"><?php esc_html_e('Upload Message', 'usp'); ?></label></th>
										<td><textarea class="textarea" rows="3" cols="50" name="usp_options[upload-message]"><?php if (isset($usp_options['upload-message'])) echo esc_textarea($usp_options['upload-message']); ?></textarea>
										<div class="mm-item-caption"><?php esc_html_e('Message that appears next to the upload field. Useful for stating your upload guidelines/policy/etc. Basic markup allowed.', 'usp'); ?></div></td>
									</tr>
									
								</table>
							</div>
							
							<input type="submit" class="button-primary" value="<?php esc_attr_e('Save Settings', 'usp'); ?>" />
						</div>
					</div>
					
					<div id="mm-panel-secondary" class="postbox"  style="padding: 20px">
						<h2>Показать форму</h2>
						<div class="toggle default-hidden">
							
							<h3>Форма после отправки</h3>
							<p>USP позволяет вам отображать форму после отправки в любом месте вашего сайта.</p>
							<p>Используйте короткий код для отображения формы на любой странице WP или странице:</p>
							<p><code class="mm-code">[user-submitted-posts]</code></p>
							<p>Или используйте тег шаблона для отображения формы в любом месте шаблона темы:</p>
							<p><code class="mm-code">&lt;?php if (function_exists('user_submitted_posts')) user_submitted_posts(); ?&gt;</code></p>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	
	<script type="text/javascript">
		jQuery(document).ready(function($){
			
			// dismiss alert
			if (!$('.dismiss-alert-wrap input').is(':checked')){
				$('.dismiss-alert-wrap input').one('click', function(){
					$('.dismiss-alert-wrap').after('<input type="submit" class="button-secondary" value="<?php esc_attr_e('Save Preference', 'usp'); ?>" />');
				});
			}
			
			// prevent accidents
			if (!$("#mm_restore_defaults").is(":checked")){
				$('#mm_restore_defaults').click(function(event){
					var r = confirm("<?php esc_html_e('Are you sure you want to restore all default options? (this action cannot be undone)', 'usp'); ?>");
					if (r == true) $("#mm_restore_defaults").attr('checked', true);
					else $("#mm_restore_defaults").attr('checked', false);
				});
			}
			
		});
	</script>

<?php }
